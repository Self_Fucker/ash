import {createRouter, createWebHashHistory} from 'vue-router';
import Ash from './components/Ash.vue';

const routes = [
  {path: '/', name: 'Ash', component: Ash},
];

export default createRouter({
  routes,
  history: createWebHashHistory(),
});
