const yaml = require('yaml');
const fs = require('fs');
const path = require('path');

// export const ashPath = 'D:/AzurLaneAutoScript';
export const ashPath = import.meta.env.MODE === 'development' ? 'C:/Users/malah/Documents/azAutoS' : process.cwd();
// export const ashPath = process.cwd();

const file = fs.readFileSync(path.join(ashPath, './config/deploy.yaml'), 'utf8');
const config = yaml.parse(file);
const PythonExecutable = config.Deploy.Python.PythonExecutable;
const WebuiPort = config.Deploy.Webui.WebuiPort.toString();

export const pythonPath = (path.isAbsolute(PythonExecutable) ? PythonExecutable : path.join(ashPath, PythonExecutable));
export const webuiUrl = `http://127.0.0.1:${WebuiPort}`;
export const webuiPath = 'gui.py';
export const webuiArgs = ['--port', WebuiPort];
